const Product = require("../models/Product.js");

//====================================CREATE A NEW PRODUCT==================================================
module.exports.addProduct = (data) => {
    console.log(data.isAdmin);
    if(data.isAdmin) {
        let newProduct = new Product({
            name: data.product.name,
            description: data.product.description,
            imgSrc: data.product.imgSrc,
            price: data.product.price,
            stocks: data.product.stocks,
            isActive: data.product.isActive
        })

        return newProduct.save().then((product, error) => {
            if(error){
                return error;
            }
            return product;
        })
    } 

    let message = Promise.resolve("You are not authorized to perform this action.");

    return message.then((value) => {
        return value
    })
}


// =================RETRIEVE ALL PRODUCTS (Admin is only Authorized to Retrieve All Products)==========


module.exports.getAllProducts = (reqBody,isAdmin) => {
	
	if(isAdmin) {
		return Product.find({}).then(result => {
			return result;
		})
	}
	let message = Promise.resolve("You are not authorized to perform this action.");
	 return message.then((value) => {
        return value;
    })	
	
	

}


// ==============RETRIEVE ALL ACTIVE PRODUCTS========|(All users are allowed to view/retrieve the products)=========
module.exports.getAllActive = () => {
	return Product.find({isActive : true}).then(result => {
		return result;
	})
}

// =========================RETRIEVING SPECIFIC PRODUCT===================================================
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result =>{
		return result;
	})
}

// =================================UPDATING A PRODUCT===============================================================
module.exports.updateProduct = (reqParams, reqBody, isAdmin) => {
	console.log(isAdmin);

	if(isAdmin){
		let updatedProduct = {
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
		}

		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
			if(error){
				return false;
			}else{
				return "Product Successfully Updated!";
			}
		})
	}

	// If the user is not admin, then return this message as a promise to avoid errors
	let message = Promise.resolve("You are not authorized to perform this action.");

	return message.then((value) => {
		return value
	})
	
}


// ================================ARCHIVE A PRODUCT========================================
module.exports.archiveProduct = (reqParams, isAdmin) => {
	// console.log(isAdmin);
	let updateActiveField = {
		isActive : false
	};

	if(isAdmin){
		return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {

			// Product not archived
			if (error) {
				return {isUpdated: false};
			// Product archived successfully
			} else {
				return {isUpdated: true};
			}
		});	
	}

	// If the user is not admin, then return this message as a promise to avoid errors
	let message = Promise.resolve("You are not authorized to perform this action.");

	return message.then((value) => {
		return value
	})
};

// UNARCHIVE PRODUCT
module.exports.unarchiveProduct = (reqParams, isAdmin) => {
	// console.log(isAdmin);
	let updateActiveField = {
		isActive : true
	};

	if(isAdmin){
		return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {

			// Product not archived
			if (error) {
				return {isUpdated: false};
			// Product archived successfully
			} else {
				return {isUpdated: true};
			}
		});	
	}

	// If the user is not admin, then return this message as a promise to avoid errors
	let message = Promise.resolve("You are not authorized to perform this action.");

	return message.then((value) => {
		return value
	})
};



