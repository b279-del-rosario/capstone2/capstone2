const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
    email: {
        type: String,
        required: [true, "Email is required"],
        },
    password: {
        type: String,
        required: [true, "Password is required"]
    },
    firstName: {
        type: String,
        required: [true, "First name is required"]
    },
    lastName: {
        type: String,
        required: [true, "Last name is required"]
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    orderedProduct: [{
        products: [{
            productId: {
                type: String,
                required: [true, "Is required"]
            },
            productName: {
                type: String,
                retuired: [true, "Is required"]
            },
            quantity: {
                type: Number,
                required: [true, "Is required"]
            }
        }],
        totalAmount: {
            type: Number,
            required: [true, "Is required"]
        },
        PurchasedOn: {
            type: Date,
            default: new Date()
        }
    }]
})

module.exports = mongoose.model('User', userSchema)