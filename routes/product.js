const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController.js");
const auth = require("../auth.js");


//================================ROUTE TO CREATE PRODUCT========================================
router.post("/", auth.verify, (req, res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.addProduct(data).then(resultFromController => res.send(resultFromController));
});

// ===============================RETRIEVE ALL PRODUCTS (ADMIN ONLY)===============================
router.get("/all", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	productController.getAllProducts(req.body,isAdmin).then(resultFromController => res.send(resultFromController));
});



// ==============================RETRIEVE ALL ACTIVE PRODUCTS============================================
router.get("/", (req, res) => {
	productController.getAllActive().then(resultFromController => res.send(resultFromController));
})

//============================RETRIEVE SPECIFIC PRODUCT USING (productId)==================================
router.get("/:productId", (req, res) => {
	console.log(req.params.productId);
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));

})

//=====================================UPDATE SPECIFIC PRODUCT======================================================
router.put("/:productId", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	productController.updateProduct(req.params, req.body, isAdmin).then(resultFromController => res.send(resultFromController));

})

//====================================ROUTE to ARCHIVE PRODUCT========================================================
// A "PUT" request is used instead of "DELETE" request because of our approach in archiving and hiding the courses from our users by "soft deleting" records instead of "hard deleting" records which removes them permanently from our databases
router.put("/:productId/archive", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	productController.archiveProduct(req.params, isAdmin).then(resultFromController => res.send(resultFromController));
	
});

// unarchive product
router.put("/:productId/unarchive", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	productController.unarchiveProduct(req.params, isAdmin).then(resultFromController => res.send(resultFromController));
});
















// Allows us to export the "router" object that will be accessed in our "index.js" file

module.exports = router;